insert into public.people
values (1, 'Quentin', 'Tarentino');
insert into public.people
values (2, 'Guy', 'Ritchie');
insert into public.people
values (3, 'James', 'Cameron');
insert into public.people
values (4, 'Emile', 'Ardolino');
insert into public.people
values (5, 'Brad', 'Pitt');
insert into public.people
values (6, 'Mélanie', 'Laurent');
insert into public.people
values (7, 'Chritoph', 'Waltz');
insert into public.people
values (8, 'Matthew', 'McConaughey');
insert into public.people
values (9, 'Hugh', 'Grant');
insert into public.people
values (10, 'Charlie', 'Hunnam');
insert into public.people
values (11, 'Leonardo', 'DiCaprio');
insert into public.people
values (12, 'Kate', 'Winslet');
insert into public.people
values (13, 'Billy', 'Zane');
insert into public.people
values (14, 'Jennifer', 'Grey');
insert into public.people
values (15, 'Patrick', 'Swayze');
insert into public.people
values (16, 'Jerry', 'Orbach');
insert into public.people
values (17, 'Jason', 'Statham');
insert into public.people
values (18, 'Vinnie', 'Jones');

insert into public.movies
values (1, 'Inglourious Basterds', 2009, 153, 1);
insert into public.movies
values (2, 'The Gentlemen', 2020, 113, 2);
insert into public.movies
values (3, 'Titanic', 1997, 194, 3);
insert into public.movies
values (4, 'Dirty Dancing', 1987, 160, 4);
insert into public.movies
values (5, 'Snatch', 2000, 163, 2);

insert into public.roles values (5, 1, 'Lt Aldo Raine');
insert into public.roles values (6, 1, 'Shosanna Dreyfus');
insert into public.roles values (7, 1, 'Col. Hans Landa');
insert into public.roles values (8, 2, 'Michael Pearson');
insert into public.roles values (9, 2, 'Fletcher');
insert into public.roles values (10, 2, 'Ray');
insert into public.roles values (11, 3, 'Jack Dawson');
insert into public.roles values (12, 3, 'Rose DeWitt');
insert into public.roles values (13, 3, 'Cal Hockley');
insert into public.roles values (14, 4, 'Frances "Baby" Houseman');
insert into public.roles values (15, 4, 'Johnny Castle');
insert into public.roles values (16, 4, 'Dr Jake Houseman');
insert into public.roles values (17, 5, 'Le turc');
insert into public.roles values (18, 5, 'Bullet Tooth Tony');
insert into public.roles values (5, 5, 'One Punch Mickey O''neil');

insert into public.reviewers values (1, 'hungry jang');
insert into public.reviewers values (2, 'adoring sammet');
insert into public.reviewers values (3, 'elated nobel');
insert into public.reviewers values (4, 'insane tesla');
insert into public.reviewers values (5, 'sleepy mclean');
insert into public.reviewers values (6, 'insane ride');
insert into public.reviewers values (7, 'silly babbage');
insert into public.reviewers values (8, 'lonely volhard');
insert into public.reviewers values (9, 'lonely turing');
insert into public.reviewers values (10, 'desperate knuth');

insert into public.reviews values (1, 1, 5);
insert into public.reviews values (2, 1, 3);
insert into public.reviews values (4, 1, 2);
insert into public.reviews values (5, 1, 4);
insert into public.reviews values (1, 2, 1);
insert into public.reviews values (2, 2, 3);
insert into public.reviews values (3, 2, 2);
insert into public.reviews values (4, 2, 4);
insert into public.reviews values (5, 2, 5);
insert into public.reviews values (1, 3, 4);
insert into public.reviews values (4, 3, 3);
insert into public.reviews values (5, 3, 2);
insert into public.reviews values (3, 4, 5);
insert into public.reviews values (1, 5, 1);
insert into public.reviews values (1, 7, 3);
insert into public.reviews values (2, 7, 4);
insert into public.reviews values (3, 7, 5);
insert into public.reviews values (1, 8, 5);
insert into public.reviews values (5, 8, 5);
insert into public.reviews values (4, 9, 5);
