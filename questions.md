# Découverte des bases de données relationelles

## Prérequis

### Installation de docker

On va utiliser docker en mode "baguette magique" pour démarrer postgresql. Installer docker pour windows en le téléchargeant à l'adresse suivante : https://download.docker.com/win/static/stable/x86_64/docker-20.10.14.zip


### Installation de pgAdmin

pgAdmin est une interface d'administration pour postgresql. Ce programme nous permettra d'interagir avec nos bases de données via une interface graphique, sans s'embêter avec le Java dans un premier temps.

Il permet d'écrire des requêtes SQL (le langage qui permet d'intéragi avec la plupart des bases de données relationelles) mais aussi de parcourir la base de données en cliquant dans une interface graphique.

Installer pgAdmin pour windows à l'adresse suivante : https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v6.7/windows/pgadmin4-6.7-x64.exe

### Démarrer postgresql avec docker

Docker permet de démarrer des **containers** à partir d'**images**. Une image est une sorte de "template" qui décrit la façon de lancer un programme. Le container est une instance isolée de ce programme.

Il y a une analogie possible avec la POO : Image = Classe, Container = Objet (ou instance)

Pour démarrer un container postgresql avec docker : taper la commande suivante dans un terminal : 

```
docker container run --name my-postgres -p5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

Le container peut ensuite être stoppé / démarré avec les commandes suivantes (il faudra peut-être le démarrer après un reboot de l'ordinateur) : 

```
docker container stop my-postgres
docker container start my-postgres
```

### Se connecter à la base avec pgAdmin et créer la base de test

Démarrer pgAdmin et créer une nouvelle connexion en choisissant "Create Server". Les informations de connexion sont les suivantes : 

* Host : localhost
* Port : 5432
* username : postgres
* password : le mot de passe renseigné dans la commande docker de création du container.

Ouvrir une console sur le schéma "public" et jouer les deux scripts sql fournis à l'adresse suivante : https://gitlab.com/BenoitAverty/sql-movies-exercices

Vous pouvez explorer les tables et les données créées grâce à pgAdmin

## Requêtes SQL

### Sélection simple

1) Afficher tout le contenu de la table `movies`
2) Afficher la liste des films triés par année de sortie croissante (Uniquement le nom des films doit apparaitre)
3) Afficher la liste des personnes enregistrées en base (table `people`, uniquement les noms et prénoms)
4) Afficher la liste des films sortis avant les années 2000
5) Afficher la durée moyenne des films

### Jointures

1) Afficher la liste des films avec leur réalisateur
1) Afficher la liste des films avec leur casting (Chaque film aura une ligne par personnage)
2) Afficher la liste des personnages du film "titanic" (Seulement le nom des personnages doit apparaitre)
3) Afficher la liste des personnes qui ont réalisé un film (uniquement leur nom)
4) Afficher la liste des notes du film "The Gentlemen"
5) Afficher la liste des reviewers ayant noté tous les films
6) Afficher la liste des reviewers n'ayant noté aucun film

### Grouper

1) Afficher la note moyenne de chaque film
2) Afficher la liste des films triés par la note moyenne la plus forte

### Insérer des données

1) Insérer un film réalisé par Guy Ritchie dans la base de données
2) Insérer un film réalisé par Lino Ventura dans la base de données
3) Insérer le casting de ces deux films dans la base de données

### Modifier/Supprimer des données

1) Modifier la note de "Hungry Jang" sur le film "Inglourious Basterds" à 3 étoiles
2) Supprimer la note de "Hungry Jang" sur le film "The Gentlemen"
3) Supprimer le film "Snatch" de la base de données, et toutes les données qui y sont liées

## Modélisation de base de données

On souhaite ajouter le support des séries TV à la base de données.

### Création de table

1) écrire la requête de création d'une table `tv_shows` pour stocker les séries. Cette table doit contenir les informations suivantes : 
   * un ID numérique qui est la clé primaire
   * Un titre
   * Un nombre de saisons
   * Une année de début
   * Une année de fin (potentiellement vide si la série est en cours)
2) écrire la requête de création d'une table `shows_episodes` pour stocker les épisodes d'une série. Cette table doit contenir les informations suivantes
   * Un numéro de saison
   * Un numéro d'épisode dans la saison
   * L'id de la série dont l'épisode fait partie (cette colonne doit référencer la colonne ID des séries TV)
   * Un titre
   * La clé primaire de cette table est constituée des trois premières colonnes
3) Insérer quelques données dans ces tables.
4) BONUS : créer une table permettant de stocker des reviews sur des épisodes de séries.

## Documentation

* https://docs.postgresql.fr/12/
* https://docs.postgresql.fr/12/ddl.html définition de données
* https://docs.postgresql.fr/12/dml.html modification / insertion de données
* https://docs.postgresql.fr/12/queries.html
