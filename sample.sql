drop type if exists regime;
drop table if exists animaux;
drop table if exists legumes;

----

CREATE TYPE regime AS ENUM ('carnivore', 'herbivore', 'omnivore');

create table legumes (
    tid varchar(36) primary key,
    nom text not null,
    couleur text
);

create table animaux (
    tid varchar(36) primary key,
    nom text not null,
    nombre_pattes int not null,
    legume_prefere varchar(36) references legumes(tid)
);

---

insert into legumes values('4f049223-704c-487a-ac7c-99a5bad9cbba', 'chou', 'vert');
insert into legumes values('a2b843b8-7389-4df4-9b86-f924bd7d2a7d', 'chou', 'rouge');
insert into legumes values('374a1c30-15a3-4b53-8150-8ddc581b3655', 'tomate', 'rouge');
insert into legumes values('01f7f556-7aec-42ed-9373-1b018a79ec4f', 'tomate', 'jaune');
insert into legumes values('56aee3e8-fecc-4666-be0e-2989db0f9c92', 'salade', null);
insert into legumes values('68660215-368a-4f90-b4e4-c5ab6f2a336c', 'carotte', null);

insert into animaux values('2bc94dfa-e776-4487-ad38-feef4a0376c1', 'lapin', 4, '01f7f556-7aec-42ed-9373-1b018a79ec4f');
insert into animaux values('5e82e713-ece2-4973-a7a9-619c5966bf7d', 'Âne', 4, '01f7f556-7aec-42ed-9373-1b018a79ec4f');
insert into animaux values('258bf92c-611e-45be-a2dc-3ebdb1f3ea59', 'kangourou', 2, null);
insert into animaux values('d9f59987-dd96-4d4c-8cfe-5f44c4e29bc5', 'chenille', 6, '4f049223-704c-487a-ac7c-99a5bad9cbba');
insert into animaux values('a731a743-24b2-4a94-a1b7-fa7130c4df73', 'limace', 0, '4f049223-704c-487a-ac7c-99a5bad9cbba');
